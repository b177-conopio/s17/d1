// JavaScript Array

// Array basic structure
/* Syntax:
	let/const arrayName = [elementA, elementB, elementC...];
*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerbrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of mixed elements in an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, true];

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

console.log(grades);
console.log(computerbrands[2], computerbrands[7]);
console.log(computerbrands[4]);

// Reassign array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'hello world';
console.log("Array after reassignment")
console.log(myTasks);

// Array Methods

// Mutator Methods
// -are functions that mutate or change an array after they're created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push()
// adds an element in the end of an array and return the array's length
/* Syntax:
	arrayName.push()
*/

console.log('Current array:');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from the push method');
console.log(fruits);

// pop()
// - removes the last element in an array and returns the removed element
/* Syntax:
	arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method');
console.log(fruits);

// unshift()
/*
	-adds one or more elements at the beginning of an array
	-Syntax:
		arrayName.unshift('elementA, elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

// shift()
/*
	- removes an element at the beginning of an array and returns the removed element;
	-Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);

// splice()
/*
	-simultaneously removed elements from a specified index number and adds elements
	-Syntax:
		arrayName.splice(startingIndex, deleteCount, elementToBeAdded)
*/
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method')
console.log(fruits);

// Banana, Lime, Cherry, Kiwi, Dragon Fruit

// sort
/*
	-rearranges the array elements in alphanumeric order
	-Syntax:
		arrayName.sort();
*/
fruits.sort();
console.log('Mutated array from sort method');
console.log(fruits);

// reverse()
/* 
	-reverses the order of array elements
	-Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log('Mutated array from reverse method')
console.log(fruits);

// Non-mutator methods
// -these are functions that do not modify or change an array after created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// indexOf()
/*
	-returns an index number of the first matching element found in an array
	-if no match was found, the result will be -1
	-Syntax:
		arrayName.indexOf(searchValue);
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf()
/*
	-returns the index number of the last matching element found in an array
	-Syntax:
		arrayName.lastIndexOf(search value);
		arrarName.lastIndexOf(search value, fromIndex);
*/

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// slice()
/*
	- portions/slices elements from array and returns a new array
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

// 'US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'

// Slicing of elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

// slicing elements from a specified index to another index
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

// toString()
/*
	-returns an array as a string separated by commas
	-Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);

// concat()
/*
	-combines two arrays and returns the combined result
	-Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method: ');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method: ');
console.log(combinedTasks);

// join()
/*
	-returns an array as a string separated by a specified separator string
	Syntax:
		arrayName.join('SeparatorString')
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join('-'));
console.log(users.join(''));

// Iteration
// -are loops designed to perform repetitive tasks on arrays

// forEach()
/*
	-similar to a for loop that iterates on each array element
	-Syntax:
		arrayName.forEach(funciton(indivElement)){
			statement;
		}
*/

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log("Result of filtered tasks: ");
console.log(filteredTasks);

// map()
/*
	- iterates on each element and returns a new array with different values depending on the result of the function's operation
	-Syntax
		let resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
})

console.log("Result of map method: ");
console.log(numberMap);

// every()
/*
	-Checks if all elements in an array meet the given condition
	-Syntax 
		let resultArray = arrayName.every(function(indivElement){
			return conditon;
		})
*/

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	-checks if at least one element in the array meets the given condition
	-Syntax:
		let resultArray = arrayName.some(funciton(indivElement){
			return condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log("Result of some method: ");
console.log(someValid);

// filter()
/*
	- returns a new array that contains elements which meets the given conditon
	- Syntax:
		let resultarray = arrayName.filter(function(indivElement){
			return condition;
		})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})

console.log("Result of filter method: ");
console.log(filterValid);

